(function ($, Drupal, drupalSettings) {
  Drupal.color = {
    logoChanged: false,
    callback: function callback(context, settings, $form) {
      if (!this.logoChanged) {
        $('.color-preview .color-preview-logo img').attr('src', drupalSettings.color.logo);
        this.logoChanged = true;
      }

      if (drupalSettings.color.logo === null) {
        $('div').remove('.color-preview-logo');
      }

      var $colorPreview = $form.find('.color-preview');
      var $colorPalette = $form.find('.js-color-palette');

      $colorPreview.find('.toolbar-bar').attr('style', 'background-color: ' + $colorPalette.find('input[name="palette[toolbarbg]"]').val());
      $colorPreview.find('.toolbar-bar').attr('style', 'background-color: ' + $colorPalette.find('input[name="palette[toolbartraybg]"]').val());
      $colorPreview.find('.content-header').attr('style', 'background-color: ' + $colorPalette.find('input[name="palette[contentheaderbg]"]').val());
      //$(document).find('.toolbar-bar').attr('style', 'background-color: ' + $colorPalette.find('input[name="palette[toolbarbg]"]').val());
      //$(document).find('.toolbar-tray').attr('style', 'background-color: ' + $colorPalette.find('input[name="palette[toolbartraybg]"]').val());
    }
  };
})(jQuery, Drupal, drupalSettings);