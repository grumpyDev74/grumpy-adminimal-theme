/* plugins */
var gulp = require('gulp'),
        plumber = require('gulp-plumber'),
        browsersync = require('browser-sync'),
        sass = require('gulp-sass'),
        cleanCSS = require('gulp-clean-css'),
        sourcemaps = require('gulp-sourcemaps'),
        imagemin = require('gulp-imagemin'),
        rename = require('gulp-rename'),
        concat = require('gulp-concat'),
        notify = require('gulp-notify'),
        cache = require('gulp-cache'),
        sassdoc = require('sassdoc');

/* paths */

var paths = {
    src: ''
};

var assets = {
    css: paths.src + 'css',
    scss: paths.src + 'scss',
    img: paths.src + 'images'
};

// url of your project
var urlSync = 'http://grumpal.docker.localhost:8000';

/* remove print css from concatenation + Concatenate & Minify CSS */
gulp.task('sass', function () {
    var all = gulp.src([assets.scss + '/**/*.scss',
            assets.scss + '/*.scss'])
            .pipe(plumber())
            .pipe(sass().on('error', sass.logError))
            .pipe(sourcemaps.init())
            .pipe(concat('all.css'))
            .pipe(gulp.dest(assets.css))
            .pipe(notify({message: 'all.css generated', onLast: true}))
            .pipe(cleanCSS())
            .pipe(rename('style.css'))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(assets.css))
            .pipe(browsersync.reload({stream: true}))
            .pipe(notify({message: 'style.css (minified) generated', onLast: true}));

    return all;
});

// sassdoc
gulp.task('sassdoc', function () {
  var options = {
    dest: 'sassdoc',
    verbose: true,
    display: {
      access: ['public', 'private'],
      alias: true,
      watermark: true,
    },
    basePath: 'https://github.com/SassDoc/sassdoc',
  };

  return gulp.src(assets.scss + '/**/*.scss')
      .pipe(sassdoc(options));
});

/* Optimise images */
gulp.task('images', function () {
    return gulp.src(assets.img + '/**/*')
            .pipe(plumber())
            .pipe(cache(imagemin({optimizationLevel: 3, progressive: true, interlaced: true})))
            .pipe(gulp.dest(assets.img));
});

/* browsersync */
gulp.task('browser-sync', function () {
    browsersync.init({
        proxy: urlSync,
        port: 3000
    });
});

gulp.task('browsersync-reload', function () {
    browsersync.reload();
});

// Watch Files For Changes
gulp.task('watch', gulp.series('browser-sync'), function () {
    gulp.watch(assets.scss + '/**/*.scss', ['sass', 'sassdoc']);
    gulp.watch(assets.img + '/**/*', ['images']);
});

gulp.task('sass-suite', gulp.series(gulp.parallel('sass', 'sassdoc')));

// Default Task
gulp.task('default', gulp.series(gulp.parallel('watch', 'sass-suite')));
